---
date: 2014-09-28
linktitle: Cryptography and Coins at 2^6 + 2^0
title:
weight: 10
---

### Celebrating Silvio Micali's 65th birthday
October 3-4, 2019, Boston, MA

This workshop is organized in honor of Silvio Micali, on the occasion of his 65th birthday. Silvio made numerous fundamental contributions in cryptography, game-theory, and the connection between them. A very partial list includes inventing or revolutionizing such key concepts as probabilistic encryption, coin-flipping and pseuforandomness, byzantine agreement, signatures, zero-knowledge, secure computation, rational proofs and rational secure computation, resilient mechanism design, and many others.

The workshop consists of technical sessions on both days, and an evening social events on the first day.

<a href="https://forms.gle/mmt5SikXNsnaNRG59">Register to attend</a>

### Speakers

|  |  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;   |  |
|------------|-------------|-------|
| | Manuel Blum (CMU) 
| | Jing Chen (Stony Brook, Algorand) 
| | Alessandro Chiesa (UC Berkeley) 
| | Rosario Gennaro (CUNY (City College)) 
| | Oded Goldreich (Weizmann Institute) 
| | Shafi Goldwasser (MIT, Weizmann Institute, UC Berkeley) 
| | Shai Halevi (Algorand Foundation) 
| | Rafail Ostrovsky (UCLA) 
| | Rafael Pass (Cornell Tech) 
| | Chris Peikert (Univ. of Michigan (Ann Arbor)) 
| | Tal Rabin (Algorand Foundation) 
| | Leo Reyzin (BU) 
| | Ron Rivest (MIT) 
| | Alon Rosen (IDC) 
| | abhi shelat (Northeastern) 
| | Paul Valiant (Brown) 
| | Avi Wigderson (IAS)

### Venue



### Organizers

    Jing Chen - Stony Brook, Algorand
    Shai Halevi - Algorand Foundation
    Tal Rabin - Algorand Foundation
    Leo Reyzin - BU
    Alon Rosen - IDC

